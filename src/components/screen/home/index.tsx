import * as React from "react";
import ImageSlider from "../../layouts/Slider";
import Categories from "../../categories";

export const Home: React.SFC<any> = () => {
    return (<div>
        <ImageSlider/>
        <Categories/>
    </div>);
};