import * as React from "react";
import Header from "../../layouts/Header";
import ImageSlider from "../../layouts/Slider";
export const Home: React.SFC<any> = () => {
    return(<div>
        <Header/>
        <div className="banner-div">
            <ImageSlider/>
        </div>
    </div>) ;
};