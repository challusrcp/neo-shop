import * as React from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router-dom";
import ROUTES from "../../lib/constants/routes";
import '../../lib/css/footer.css'

interface State {
}

interface OwnProps {
}

interface DispatchProps {
}

interface StateProps {
}

type Props = StateProps & OwnProps & DispatchProps

class Footer extends React.Component<Props, State> {

    constructor(prop: Props) {
        super(prop);
        this.state = {}
    }

    render() {
        return (
            <footer>
                <div className="container footer-block">
                    <div className="row">
                        <div className="col-md-2">
                            <h6>ONLINE SHOPPING</h6>
                            <Link to={ROUTES.MEN} className="d-block">
                                Men
                            </Link>
                            <Link to={ROUTES.WISH_LIST} className="d-block">
                                Women
                            </Link>
                            <Link to={ROUTES.KIDS} className="d-block">
                                Kids
                            </Link>
                            <Link to={ROUTES.HOME_LIVING} className="d-block">
                                Home & Living
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Discover
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Gift Cards
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Myntra Insider <span className="badge badge-pill badge-danger">New</span>
                            </Link>
                        </div>
                        <div className="col-md-2">
                            <h6>USEFUL LINKS</h6>
                            <Link to={ROUTES.MEN} className="d-block">
                                FAQ
                            </Link>
                            <Link to={ROUTES.WISH_LIST} className="d-block">
                                T&C
                            </Link>
                            <Link to={ROUTES.KIDS} className="d-block">
                                Terms Of Use
                            </Link>
                            <Link to={ROUTES.HOME_LIVING} className="d-block">
                                Track Orders </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Shipping
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Cancellation
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Returns
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Whitehat
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Blog
                            </Link>
                            <Link to={ROUTES.DISCOVER} className="d-block">
                                Careers
                            </Link>
                        </div>

                        <div className="col-md-4">
                            <h6>EXPERIENCE MYNTRA APP ON MOBILE</h6>
                            <h6>KEEP IN ZONE</h6>
                            <div className="footer-icons">
                                <i className="fab fa-facebook-square"/>
                                <i className="fab fa-twitter"/>
                                <i className="fab fa-youtube"/>
                                <i className="fab fa-instagram"/>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <p><strong>100% ORIGINAL</strong> guarantee for all products at myntra.com</p>
                            <p><strong>Return within 30days</strong> of receiving your order</p>
                            <p><strong>Get free delivery</strong> for every order above Rs. 1199</p>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}


export default connect()(Footer)
