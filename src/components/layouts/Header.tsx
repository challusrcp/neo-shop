import * as React from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router-dom";
import ROUTES from "../../lib/constants/routes";
import '../../lib/css/header.css'

const logo = require("../../lib/img/brand-logo.jpg");

interface State {
}

interface OwnProps {
}

interface DispatchProps {
}

interface StateProps {
}

type Props = StateProps & OwnProps & DispatchProps

class Header extends React.Component<Props, State> {

    constructor(prop: any) {
        super(prop);
        this.state = {}
    }

    render() {
        return (
            <div className="header-block">
                <nav className="navbar navbar-expand-lg navbar-light fixed-top py-3 px-5 navbar-scrolled " id="mainNav">
                    <div className="container-fluid">
                        <Link to={ROUTES.HOME} className="navbar-brand js-scroll-trigger">
                            <img src={logo} alt="missing" className="brand-icon"/>
                        </Link>
                        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav my-2 my-lg-0 neo-head-menu">
                                <li className="nav-item">
                                    <Link to={ROUTES.MEN} className="nav-link js-scroll-trigger">
                                        MEN
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.WISH_LIST} className="nav-link js-scroll-trigger">
                                        WOMEN
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.KIDS} className="nav-link js-scroll-trigger">
                                        KIDS
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.HOME_LIVING} className="nav-link js-scroll-trigger">
                                        HOME & LIVING
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.DISCOVER} className="nav-link js-scroll-trigger">
                                        DISCOVER
                                    </Link>
                                </li>
                            </ul>
                            <ul className="navbar-nav ml-auto  my-2 my-lg-0">
                                <form className="form-inline ">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text"><i className="fa fa-search"/></span>
                                        </div>
                                        <input className="form-control mr-sm-2" type="text"
                                               placeholder="Search for products"/>
                                    </div>
                                </form>
                                <li className="nav-item">
                                    <Link to={ROUTES.USER} className="nav-link js-scroll-trigger">
                                        <div className="d-flex justify-content-center"><i className="far fa-user"/>
                                        </div>
                                        Profile
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.WISH_LIST} className="nav-link js-scroll-trigger">
                                        <div className="d-flex justify-content-center"><i className="far fa-bookmark"/>
                                        </div>
                                        Wishlist
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={ROUTES.BAG} className="nav-link js-scroll-trigger">
                                        <div className="d-flex justify-content-center"><i
                                            className="fa fa-shopping-bag"/></div>
                                        Bag
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}


export default connect()(Header)
