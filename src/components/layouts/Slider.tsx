import * as React from 'react'
import {connect} from 'react-redux'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import '../../lib/css/slider.css'
import {Carousel} from 'react-responsive-carousel';


interface State {
}

interface OwnProps {
}

interface DispatchProps {
}

interface StateProps {
}

type Props = StateProps & OwnProps & DispatchProps

class ImageSlider extends React.Component<Props, State> {

    constructor(prop: Props) {
        super(prop);
        this.state = {}
    }

    render() {
        return (<div className="img-slider container-fluid">
                <Carousel showThumbs={false} autoPlay={true} infiniteLoop={true}>
                    <div>
                        <img src="https://www.w3schools.com/bootstrap4/la.jpg" alt="image1"/>
                    </div>
                    <div>
                        <img src="https://www.w3schools.com/bootstrap4/chicago.jpg" alt="image2"/>
                    </div>
                    <div>
                        <img src="https://www.w3schools.com/bootstrap4/ny.jpg" alt="image3"/>
                    </div>
                </Carousel>
            </div>
        )
    }
}


export default connect()(ImageSlider)
