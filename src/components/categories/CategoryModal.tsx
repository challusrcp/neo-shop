import React from 'react';
import Modal from 'react-bootstrap/Modal'
import {Button} from 'react-bootstrap';


interface State {
    modalIsOpen: boolean,
    categoryText: string,
    isValidText: boolean
}

interface Props {
    modalHandler: any,
    openModal: boolean,
    addCategory: any
}


export default class CategoryModal extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            modalIsOpen: false,
            categoryText: '',
            isValidText: true
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    handleChange = (e: any) => {
        let text = e.target.value || '';
        this.setState({
            categoryText: text,
            isValidText: text.trim().toString() !== ''
        })
    };

    addCategorySubmission = () => {
        let category = this.state.categoryText;
        if (category.trim().toString()) {
            this.props.addCategory(category)
        }
    };

    render() {
        let {openModal, modalHandler} = this.props;
        let {categoryText} = this.state;
        return (
            <div>
                <Modal show={openModal} onHide={modalHandler}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Category :</label>
                                <input type="text"
                                       className={`form-control ${!categoryText && 'is-invalid'}`}
                                       value={categoryText}
                                       placeholder="Category name..."
                                       onChange={this.handleChange}
                                />
                                <div className="invalid-feedback">
                                    Please enter something as a category name.
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={modalHandler} variant="danger">
                            Cancel
                        </Button>
                        <Button onClick={this.addCategorySubmission}>
                            Submit
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

