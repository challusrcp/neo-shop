import ImageGallery from 'react-image-gallery';
import * as React from "react";

interface Props {
    images: []
}

export default class Product extends React.Component<Props> {

    render() {

        const images = this.props.images.map((image) => ({
            original: image
        }));

        return (
            <ImageGallery items={images}
                          showThumbnails={false}
                          showPlayButton={false}
                          showBullets={true}
                          showFullscreenButton={false}
                          showIndex={true}/>
        );
    }

}