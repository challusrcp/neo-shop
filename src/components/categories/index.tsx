import * as React from 'react'
import {connect} from 'react-redux'
import '../../lib/css/categories.css'
import CATEGORIES from "../../lib/constants/categories";
import CategoryModal from "./CategoryModal";
import Product from "./Product";
import {getCategories, selectCategory, unSelectCategory} from "../../actions";
import Loader from 'react-loader';


interface State {
    openModal: boolean,
    selectedCategory: any,
    products: any
}

interface DispatchProps {
    getCategories: any,
    selectCategory: any,
    unSelectCategory: any

}

interface StateProps {
    categories: any,
    loading: boolean,
    selectedCategory: any,
    products: any
}

type Props = StateProps & DispatchProps

class Categories extends React.Component<Props, State> {

    constructor(prop: Props) {
        super(prop);
        this.state = {
            openModal: false,
            selectedCategory: '',
            products: {}
        }
    }

    componentDidMount(): void {
        this.props.getCategories();
    }

    addCategoryModalHandler = () => {
        this.setState({
            openModal: !this.state.openModal
        })
    };

    addCategory = () => {
        this.addCategoryModalHandler()
    };

    onBackToCategory = () => {
        this.props.unSelectCategory();
        this.setState({
            selectedCategory: '',
            products: []
        })
    };

    onCategorySelection = (category: any) => {
        this.props.selectCategory(category);
        /* this.setState({
             selectedCategory: category
         });
         userService.getProducts().then((res: any) => {
             let item = res.data.data.filter((category: any) => category._id === this.state.selectedCategory._id)[0] || [];
             this.setState({
                 products: item
             });
         });*/
    };

    render() {
        let {openModal} = this.state;
        let {categories, loading, selectedCategory, products} = this.props;
        return (
            <div className="category-group">
                <div className="d-flex justify-content-between bd-highlight add-btn-row">
                    {selectedCategory && <i className="fas fa-arrow-circle-left" onClick={this.onBackToCategory}/>}
                    <strong>{selectedCategory ? selectedCategory.name : 'Categories'}</strong>
                    <button type="button" className="btn btn-secondary" onClick={this.addCategoryModalHandler}>Add +
                    </button>
                </div>
                <CategoryModal modalHandler={this.addCategoryModalHandler}
                               addCategory={this.addCategory}
                               openModal={openModal}/>
                {loading ? <Loader loaded={false} className="spinner"/> :
                    <div> {(selectedCategory && products.brands) ? <div className="row product-row">
                        {products.brands.map((brand: any, key: number) => <div className="col-md-4 product-div"
                                                                               key={key}>
                            <div className="box">
                                <Product images={brand.product.images}/>
                            </div>
                            <div className="card-body">
                                <h5 className="card-title text-center">{brand.name}</h5>
                                <h5 className="card-title text-center">{brand.product.name}</h5>
                            </div>
                        </div>)}
                    </div> : <div className="card-deck">
                        {categories.map((category: any, key: any) => <div className="card col-md-4 category-card"
                                                                          key={key}
                                                                          onClick={() => this.onCategorySelection(category)}>
                            <img className="card-img-top"
                                 src={CATEGORIES[category.slug]}
                                 alt={category.name}/>
                            <div className="card-body">
                                <h5 className="card-title text-center">{category.name}</h5>
                            </div>
                        </div>)}
                    </div>}</div>}

            </div>
        )
    }
}

const mapStateToProps = (states: any): StateProps => {
    const {categoryApp} = states;
    return categoryApp
};


export default connect<StateProps, DispatchProps>(mapStateToProps, {
    getCategories,
    selectCategory,
    unSelectCategory,
})(Categories)

