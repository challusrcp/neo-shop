import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Home} from "./components/screen/home";
import ROUTES from "./lib/constants/routes";
import Header from "./components/layouts/Header";
import {Bag} from "./components/screen/bag";
import Footer from "./components/layouts/Footer";

class App extends Component {

    webRender = () => {
        return (
            <Switch>
                <Route exact path={ROUTES.HOME} component={Home}/>
                <Route exact path={ROUTES.MEN} component={Home}/>
                <Route exact path={ROUTES.WOMEN} component={Home}/>
                <Route exact path={ROUTES.KIDS} component={Home}/>
                <Route exact path={ROUTES.HOME_LIVING} component={Home}/>
                <Route exact path={ROUTES.DISCOVER} component={Home}/>
                <Route exact path={ROUTES.WISH_LIST} component={Home}/>
                <Route exact path={ROUTES.BAG} component={Bag}/>
                <Route exact path={ROUTES.SIGN_UP} component={Home}/>
                <Route exact path={ROUTES.LOGIN} component={Home}/>
                <Route exact path={ROUTES.USER} component={Home}/>
            </Switch>
        );
    };

    render() {
        return (
            <Router>
                <Header/>
                <div className="content-layout">
                    <Route render={this.webRender}/>
                </div>
                <Footer/>
            </Router>
        );
    }
}

export default App;
