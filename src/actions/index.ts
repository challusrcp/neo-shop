// TypeScript infers that this function is returning SendMessageAction
import {
    GET_CATEGORIES_FAILURE,
    GET_PRODUCTS_FAILURE,
    GETTING_CATEGORIES,
    GOT_CATEGORIES,
    SELECTED_CATEGORY,
    SET_PRODUCTS, UN_SELECTED_CATEGORY
} from "./types";
import {userService} from "../services/categoryServices";

export function getCategories() {
    return (dispatch: any) => {
        dispatch({
            type: GETTING_CATEGORIES,
        });
        userService.get().then((res: any) => {
            dispatch({
                type: GOT_CATEGORIES,
                payload: res.data.data
            })
        }).catch(() => {
            dispatch({
                type: GET_CATEGORIES_FAILURE,
            })
        })
    }
}

export function selectCategory(selectedCategory: any) {
    return (dispatch: any) => {
        dispatch({
            type: SELECTED_CATEGORY,
            payload: selectedCategory
        });
        userService.getProducts().then((res: any) => {
            let categoryObj = res.data.data.filter((category: any) => category._id === selectedCategory._id)[0] || [];
            dispatch({
                type: SET_PRODUCTS,
                payload: categoryObj
            })
        }).catch(() => {
            dispatch({
                type: GET_PRODUCTS_FAILURE,
            })
        })
    }
}

export const unSelectCategory = () => ({
    type: UN_SELECTED_CATEGORY,
});