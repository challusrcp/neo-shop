const initialState = {
    categoryState: {
        loading: false,
        categories: [],
        products: [],
        selectedCategory: ''
    }
};

export default initialState;