import initialState from "./initialState";
import {
    GET_CATEGORIES_FAILURE,
    GETTING_CATEGORIES,
    GOT_CATEGORIES,
    SELECTED_CATEGORY, SET_PRODUCTS,
    UN_SELECTED_CATEGORY
} from "../../actions/types";

export function categoryReducer(
    state = initialState.categoryState,
    action: any
): any {
    switch (action.type) {

        case GETTING_CATEGORIES: {
            return {
                ...state,
                loading: true
            }
        }

        case GOT_CATEGORIES: {
            return {
                ...state,
                loading: false,
                categories: action.payload
            }
        }

        case GET_CATEGORIES_FAILURE: {
            return {
                ...state,
                loading: false
            }
        }

        case SELECTED_CATEGORY: {
            return {
                ...state,
                loading: true,
                selectedCategory: action.payload
            }
        }
        case UN_SELECTED_CATEGORY: {
            return {
                ...state,
                selectedCategory: '',
                products: []
            }
        }

        case SET_PRODUCTS: {
            return {
                ...state,
                loading: false,
                products: action.payload
            }
        }

        default:
            return state
    }
}