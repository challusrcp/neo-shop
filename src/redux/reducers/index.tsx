import {combineReducers} from "redux";
import {categoryReducer} from "./categoriesReducer";

const rootReducer = combineReducers({categoryApp: categoryReducer});

export default rootReducer;
