import axios from 'axios';
import {BASE_API_URL} from "./api";

export const userService = {
    get,
    getProducts
};

function get() {
    return axios.get(BASE_API_URL + "/categories").then((response) => {
        return response;
    }).catch((err) => {
        console.log(err);
    })
}

function getProducts() {
    return axios.get(BASE_API_URL + "/categories/dashboard-products").then((response) => {
        return response;
    }).catch((err) => {
        console.log(err);
    })
}

