const ROUTES = {
    HOME: '/',
    LOGIN: '/login',
    SIGN_UP: '/signup',
    LOGOUT: '/logout',
    MEN: '/shop/men',
    WOMEN: '/shop/women',
    KIDS: '/shop/kids',
    HOME_LIVING: '/shop/home-living',
    DISCOVER: '/shop/discover',
    USER: '/my/profile',
    WISH_LIST: '/wish-list',
    BAG: '/checkout/cart'
};

export default ROUTES;