const CATEGORIES: any = {
    mobile: 'https://i.gadgets360cdn.com/large/redmi_note_8_pro_teaser_weibo_small_1566555552794.jpg?downsize=224:170&output-quality=80&output-format=webp',
    laptop: 'https://i.gadgets360cdn.com/large/lggram17_small_1566546710897.jpg?downsize=224:170&output-quality=80&output-format=webp',
    televisions: 'https://i.gadgets360cdn.com/large/mi_tv_4x_pro_55_thumb_1566196162228.jpg?downsize=234:176&output-quality=80&output-format=webp'
};

export default CATEGORIES;